CREATE DATABASE  IF NOT EXISTS `tgelf` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `tgelf`;
-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: tgelf
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `filled` tinyint(1) NOT NULL DEFAULT '0',
  `country_id` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'United Arab Emirates',0,'AE'),(2,'Afghanistan',0,'AF'),(3,'Albania',0,'AL'),(4,'Armenia',0,'AM'),(5,'Angola',0,'AO'),(6,'Argentina',0,'AR'),(7,'Austria',0,'AT'),(8,'Australia',0,'AU'),(9,'Azerbaijan',0,'AZ'),(10,'Bosnia and Herzegovina',0,'BA'),(11,'Bangladesh',0,'BD'),(12,'Belgium',0,'BE'),(13,'Burkina Faso',0,'BF'),(14,'Bulgaria',0,'BG'),(15,'Burundi',0,'BI'),(16,'Benin',0,'BJ'),(17,'Brunei Darussalaml',0,'BN'),(18,'Bolivia',0,'BO'),(19,'Brazil',0,'BR'),(20,'Bahamas',0,'BS'),(21,'Bhutan',0,'BT'),(22,'Botswana',0,'BW'),(23,'Belarus',0,'BY'),(24,'Belize',0,'BZ'),(25,'Canada',0,'CA'),(26,'Democratic Republic of Cong',0,'CD'),(27,'Central African Republicc',0,'CF'),(28,'Republic of Congol',0,'CG'),(29,'Switzerland',0,'CH'),(30,'Ivory Coast',0,'CI'),(31,'Chile',0,'CL'),(32,'Cameroon',0,'CM'),(33,'China',0,'CN'),(34,'Colombia',0,'CO'),(35,'Costa Rica',0,'CR'),(36,'Cuba',0,'CU'),(37,'Cyprus',0,'CY'),(38,'Czech Republic',0,'CZ'),(39,'Germany',1,'DE'),(40,'Djibouti',0,'DJ'),(41,'Denmark',0,'DK'),(42,'Dominican Republic',0,'DO'),(43,'Algeria',0,'DZ'),(44,'Ecuador',0,'EC'),(45,'Estonia',0,'EE'),(46,'Egypt',0,'EG'),(47,'Western Sahara',0,'EH'),(48,'Eritrea',0,'ER'),(49,'Spain',0,'ES'),(50,'Ethiopia',0,'ET'),(51,'Falkland Islandsla',0,'FK'),(52,'Finland',0,'FI'),(53,'Fiji',0,'FJ'),(54,'France',0,'FR'),(55,'Gabon',0,'GA'),(56,'United Kingdom',0,'GB'),(57,'Georgia',0,'GE'),(58,'French Guiana',0,'GF'),(59,'Ghana',0,'GH'),(60,'Greenland',0,'GL'),(61,'Gambia',0,'GM'),(62,'Guinea',0,'GN'),(63,'Equatorial Guineal',0,'GQ'),(64,'Greece',0,'GR'),(65,'Guatemala',0,'GT'),(66,'Guinea-Bissau',0,'GW'),(67,'Guyana',0,'GY'),(68,'Honduras',0,'HN'),(69,'Croatia',0,'HR'),(70,'Haiti',0,'HT'),(71,'Hungary',0,'HU'),(72,'Indonesia',0,'ID'),(73,'Ireland',0,'IE'),(74,'Israel',0,'IL'),(75,'India',1,'IN'),(76,'Iraq',0,'IQ'),(77,'Iran',0,'IR'),(78,'Iceland',0,'IS'),(79,'Italy',0,'IT'),(80,'Jamaica',0,'JM'),(81,'Jordan',0,'JO'),(82,'Japan',0,'JP'),(83,'Kenya',0,'KE'),(84,'Kyrgyzstan',0,'KG'),(85,'Cambodia',0,'KH'),(86,'North Korea',0,'KP'),(87,'South Korea',0,'KR'),(88,'Kosovo',0,'XK'),(89,'Kuwait',0,'KW'),(90,'Kazakhstan',0,'KZ'),(91,'Lao People\'s Democratic Rep',0,'LA'),(92,'Lebanon',0,'LB'),(93,'Sri Lanka',0,'LK'),(94,'Liberia',0,'LR'),(95,'Lesotho',0,'LS'),(96,'Lithuania',0,'LT'),(97,'Luxembourg',0,'LU'),(98,'Latvia',0,'LV'),(99,'Libya',0,'LY'),(100,'Morocco',0,'MA'),(101,'Moldova',0,'MD'),(102,'Montenegro',0,'ME'),(103,'Madagascar',0,'MG'),(104,'Macedonia',0,'MK'),(105,'Mali',0,'ML'),(106,'Myanmar',0,'MM'),(107,'Mongolia',0,'MN'),(108,'Mauritania',0,'MR'),(109,'Malawi',0,'MW'),(110,'Mexico',0,'MX'),(111,'Malaysia',0,'MY'),(112,'Mozambique',0,'MZ'),(113,'Namibia',0,'NA'),(114,'New Caledonia',0,'NC'),(115,'Niger',0,'NE'),(116,'Nigeria',0,'NG'),(117,'Nicaragua',0,'NI'),(118,'Netherlands',0,'NL'),(119,'Norway',0,'NO'),(120,'Nepal',0,'NP'),(121,'New Zealand',0,'NZ'),(122,'Oman',0,'OM'),(123,'Panama',0,'PA'),(124,'Peru',0,'PE'),(125,'Papua New Guineala',0,'PG'),(126,'Philippines',0,'PH'),(127,'Poland',0,'PL'),(128,'Pakistan',0,'PK'),(129,'Puerto Rico',0,'PR'),(130,'Palestinian Territoriescl',0,'PS'),(131,'Portugal',0,'PT'),(132,'Paraguay',0,'PY'),(133,'Qatar',0,'QA'),(134,'Romania',0,'RO'),(135,'Serbia',0,'RS'),(136,'Russia',0,'RU'),(137,'Rwanda',0,'RW'),(138,'Saudi Arabia',0,'SA'),(139,'Solomon Islandslan',0,'SB'),(140,'Sudan',0,'SD'),(141,'Sweden',0,'SE'),(142,'Slovenia',0,'SI'),(143,'Svalbard and Jan Mayencla',0,'SJ'),(144,'Slovakia',0,'SK'),(145,'Sierra Leone',0,'SL'),(146,'Senegal',0,'SN'),(147,'Somalia',0,'SO'),(148,'Suriname',0,'SR'),(149,'South Sudan',0,'SS'),(150,'El Salvador',0,'SV'),(151,'Syria',0,'SY'),(152,'Swaziland',0,'SZ'),(153,'Chad',0,'TD'),(154,'French Southern and Antarct',0,'TF'),(155,'Togo',0,'TG'),(156,'Thailand',0,'TH'),(157,'Tajikistan',0,'TJ'),(158,'Timor-Leste',0,'TL'),(159,'Turkmenistan',0,'TM'),(160,'Tunisia',0,'TN'),(161,'Turkey',0,'TR'),(162,'Trinidad and Tobago',0,'TT'),(163,'Taiwan',0,'TW'),(164,'Tanzania',0,'TZ'),(165,'Ukraine',0,'UA'),(166,'Uganda',0,'UG'),(167,'United States',1,'US'),(168,'Uruguay',0,'UY'),(169,'Uzbekistan',0,'UZ'),(170,'Venezuela',0,'VE'),(171,'Vietnam',0,'VN'),(172,'Vanuatu',0,'VU'),(173,'Yemen',0,'YE'),(174,'South Africa',0,'ZA'),(175,'Zambia',0,'ZM'),(176,'Zimbabwe',0,'ZW');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_links`
--

DROP TABLE IF EXISTS `social_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `linkedin` varchar(255) DEFAULT NULL,
  `youtube` varchar(255) DEFAULT NULL,
  `github` varchar(255) DEFAULT NULL,
  `academia` varchar(255) DEFAULT NULL,
  `behance` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_links`
--

LOCK TABLES `social_links` WRITE;
/*!40000 ALTER TABLE `social_links` DISABLE KEYS */;
INSERT INTO `social_links` VALUES (2,8,'https://twitter.com/thakurrick','https://twitter.com/thakurrick','https://twitter.com/thakurrick','https://twitter.com/thakurrick','https://twitter.com/thakurrick','https://twitter.com/thakurrick'),(3,9,'','','','','','');
/*!40000 ALTER TABLE `social_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unique_codes`
--

DROP TABLE IF EXISTS `unique_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unique_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `used` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unique_codes`
--

LOCK TABLES `unique_codes` WRITE;
/*!40000 ALTER TABLE `unique_codes` DISABLE KEYS */;
INSERT INTO `unique_codes` VALUES (1,'CvWZdYtF2z',1),(2,'K87wSEqZaT',1),(3,'SFP8TbRXhG',0),(4,'XADp5dZCfv',0),(5,'N3PKm6sL5n',0),(6,'e67FPk9RmR',0),(7,'ajc7jjyYcd',0),(8,'6woUGwYwvC',0),(9,'Ytsv2LK96w',1),(10,'WGD5gYERpj',0),(11,'8pAZ8y2ddX',0),(12,'oBMEfJuYiF',0),(13,'5GfyNYs525',0),(14,'BfqgRqzUfg',0),(15,'YTBnjvAuXN',0),(16,'PJ6C3vgkT5',0),(17,'tuFiGMxebd',0),(18,'bFrY3zKTec',0),(19,'RjgadV6BnJ',0),(20,'NXiDUp5zxd',0);
/*!40000 ALTER TABLE `unique_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_addresses`
--

DROP TABLE IF EXISTS `user_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `ispresent` tinyint(1) DEFAULT NULL,
  `addressline1` varchar(45) DEFAULT NULL,
  `addressline2` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `pincode` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_addresses`
--

LOCK TABLES `user_addresses` WRITE;
/*!40000 ALTER TABLE `user_addresses` DISABLE KEYS */;
INSERT INTO `user_addresses` VALUES (5,8,1,'6/4 ','west patel nagar','new delhi','delhi','India','110008'),(6,8,0,'6/4 ','eastpatek nagar','new delhi','delhi','India','110008'),(7,9,1,'64','6/4','Delhi','Delhi','India','11008'),(8,9,0,'#87,4th cross,muniyappa layout','Behind SEA College,Basvanapura','Bangalore','Karnataka','India','560036');
/*!40000 ALTER TABLE `user_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_interests`
--

DROP TABLE IF EXISTS `user_interests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_interests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `isprimary` tinyint(1) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_interests`
--

LOCK TABLES `user_interests` WRITE;
/*!40000 ALTER TABLE `user_interests` DISABLE KEYS */;
INSERT INTO `user_interests` VALUES (9,8,1,'Humanitarian and social sector'),(10,8,0,'Chemistry'),(11,8,0,'Mechanical Engineering'),(12,8,0,'Psychology & Behavioral Sciences'),(13,9,1,'Visual and Performing Arts'),(14,9,0,'Psychology & Behavioral Sciences'),(15,9,0,'Chemistry'),(16,9,0,'Mechanical Engineering');
/*!40000 ALTER TABLE `user_interests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_profiles`
--

DROP TABLE IF EXISTS `user_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `countrycode` varchar(45) DEFAULT NULL,
  `contactno` int(11) DEFAULT NULL,
  `personalemail` varchar(45) DEFAULT NULL,
  `professionalemail` varchar(45) DEFAULT NULL,
  `bio` varchar(255) DEFAULT NULL,
  `fathername` varchar(45) DEFAULT NULL,
  `mothername` varchar(45) DEFAULT NULL,
  `photo` varchar(45) DEFAULT NULL,
  `universitycompany` varchar(255) DEFAULT NULL,
  `majorposition` varchar(255) DEFAULT NULL,
  `funfact` varchar(255) DEFAULT NULL,
  `countrycodefather` varchar(45) DEFAULT NULL,
  `contactnofather` varchar(45) DEFAULT NULL,
  `countrycodemother` varchar(45) DEFAULT NULL,
  `contactnomother` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_profiles`
--

LOCK TABLES `user_profiles` WRITE;
/*!40000 ALTER TABLE `user_profiles` DISABLE KEYS */;
INSERT INTO `user_profiles` VALUES (3,8,'91',2147483647,'surpawan@gmail.com','pawan@theantialias.com','demo','karnail singh','usha rani','uploads/profile/91205.jpg','The Anti Alias','web developer','demo','91','8123565698','91','8123565698'),(4,9,'91',2147483647,'surpawan@gmail.com','surpawan@gmail.com','Bio','','','uploads/profile/55563.jpg','The Anti Alias','Web Developer','YOLO!!','','','','');
/*!40000 ALTER TABLE `user_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `uniquecode` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,'demo','demo','demo','$2y$10$pwNNRM4Dh5Qkuo/mtfDG..OClPdrEAQgxL7gS4w8H/SpGa1gQzyI2',NULL,''),(8,'pawan','kumar','warlock','$2y$10$udyNWAV1gqIgzZNx5on28eDokcJ9mIRVeE/eKj.gRUveHq.Hkq/by','K87wSEqZaT','QStg2w5c7WuJzfbeJcHd6XJBaP6lHNRpAlRb0Q2Ijz3HP7J6soNSpeO71Mu9'),(9,'Pawan','Kumar','ricky','$2y$10$NPscKDAX6p72E9Xv0ZqUcOczeGv0yeymNdbfpgWXjuHYasgP0bdMq','Ytsv2LK96w','UmWrM0iuT9wIYxcmPj6Uulc68Vdjyx563goKdtQzyM6XKJtR4RLcmPtGAJIp');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-17 14:09:33
