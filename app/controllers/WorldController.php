<?php

class WorldController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($countryid)
	{
		$userdata = array();
		$temp = UserAddress::where('country',$countryid)->where('ispresent','1')->get();
		foreach ($temp as $user) {
			$tempdata = User::where('id',$user->user_id)->first();
			array_push($userdata,$tempdata);
		}
		$title = "TGelf - leaders  Page";
		if(Session::has('username')){
			$username = Session::get('username');
		$user = User::where('username',$username)->first();
				$data = array(
					'title' => $title,
					'username' => $user,
					'pagecountry' => $countryid
					);

		
		return View::make('pages.showleaders',$data)->with('userdata',$userdata);
		}
		else{
		return View::make('pages.showleaders',array('title'=> $title,'pagecountry' => $countryid))->with('userdata',$userdata);
	}
	}

		public function primaryskill($skill)
	{
		$userdata = array();
		$temp = UserInterest::where('name',$skill)->where('isprimary','1')->get();
		foreach ($temp as $user) {
			$tempdata = User::where('id',$user->user_id)->first();
			array_push($userdata,$tempdata);
		}
		$title = "TGelf - leaders  Page";
		if(Session::has('username')){
			$username = Session::get('username');
		$user = User::where('username',$username)->first();
				$data = array(
					'title' => $title,
					'username' => $user,
					'pagecountry' => $countryid
					);

		
		return View::make('pages.skills',$data)->with('userdata',$userdata);
		}
		else{
		return View::make('pages.skills',array('title'=> $title,'keyskill' => $skill))->with('userdata',$userdata);
	}
	}


	public function world()
	{
		$title = "TGelf - World  Map";
		$populate = Country::where('filled', '1')->select('id')->get();
		if(Session::has('username')){
			$username = Session::get('username');
		$user = User::where('username',$username)->first();
				$data = array(
					'title' => $title,
					'username' => $user
					);
		return View::make('pages.world',$data)->with('populate',$populate);
		}
		else
		{
		$country = UserAddress::all();
		return View::make('pages.world',array('title'=> $title))->with('populate',$populate);
	}
	}

	public function fetchval()
	{	
		// $response = array();
		// $response["fetch"] = array();
		 $response = Country::get();


		 return $response;

	}
}
