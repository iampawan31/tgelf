 @extends('layouts.master')
@section('content')

        <div class="container login-container">
        <p style="text-align: center;    margin-bottom: -21px;">Leaders Forum</p>

        <h1 id="war-header" style="text-align: center;"><span id="war-header-inner">{{$pagecountry}}</span>
        </h1>
        <?php $count=0; ?>
        <div class="row">
           @foreach($userdata as $user)
           @if ($count ==2)
               <div class="row">
           @endif
                <div class="col-md-4">
                <div class="col-md-3">
                    <div id="" class="left">
                         <a href="/profile/{{$user->username}}"><img src="{{asset($user->userprofiles->photo)}}" alt="" width="75px" height="75px" style="border-radius: 50%;"></a>
                    </div>
                </div>
                <div id="right" class="col-md-9">
                    <a href="/profile/{{$user->username}}" style="color: #a83334 !important;"><h4>{{ucwords($user->firstname)}} {{ucwords($user->lastname)}}</h4></a>
                    <hr>
                   <p style="margin-bottom: 0px;">{{ucwords($user->userprofiles->universitycompany)}} | {{ucwords($user->userprofiles->majorposition)}}</p>
                    <p><span style="color:#808080">{{ucwords($user->useraddress->state)}}</span> | <span style="color:#808080;text-transform: uppercase;">{{$user->useraddress->country}}</span></p>
                </div>
                </div>
           
            <?php $count++;?>
           @endforeach
           @if ($count ==2)
               </div>
           @endif
           
        </div>
@stop
