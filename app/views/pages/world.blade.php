@extends('layouts.master')
@section('content')

<style>
a {
    color: #fff;
    }

    a:hover {
    text-decoration: underline;
    margin-top: 2% !important;
}
</style>
<div class="col-md-12 col-xs-12" id="colss" style="margin-top: 3%;position:absolute;padding: 1%;background-color: #194759;">
 <div class="row" style="text-align: center;">
    <button type="button"  class="btn btn-info1 fillt" data-toggle="collapse" data-target="#demo">FILTER BY INDUSTRY</button>
    </div>
  <div id="demo" class="collapse" style="color: #fff;    height: 100%;">
   <div class="row">
   <div class="col-md-4">
    <ul class="colps">
                        <li><a href="{{ URL::to('skills')}}/Humanitarian and social sector">Humanitarian and social sector</a></li>
                        <li><a href="{{ URL::to('skills') }}/Visual and Performing Arts">Visual and Performing Arts</a></li>
                        <li><a href="{{ URL::to('skills') }}/Economics, Business and Entrepreneurship">Economics, Business and Entrepreneurship</a></li>
                        <li><a href="{{ URL::to('skills') }}/Accounting and Finance">Accounting and Finance</a></li>
                        <li><a href="{{ URL::to('skills') }}/Biology, Health and Medicine">Biology, Health and Medicine</a></li>
                        <li><a href="{{ URL::to('skills') }}/Law and Legal Studies">Law and Legal Studies</a></li>
                    </ul>
                    
   </div>
   <div class="col-md-4">
       <ul class="colps">
                        <li><a href="{{ URL::to('skills') }}/Computer Science & Mathematics">Computer Science & Mathematics</a></li>
                        <li><a href="{{ URL::to('skills') }}/Architecture & Civil Engineering">Architecture & Civil Engineering</a></li>
                        <li><a href="{{ URL::to('skills') }}/Electrical Engineering">Electrical Engineering</a></li>
                        <li><a href="{{ URL::to('skills') }}/Mechanical Engineering">Mechanical Engineering</a></li>
                        <li><a href="{{ URL::to('skills') }}/Statistics & Actuarial Sciences">Statistics & Actuarial Sciences</a></li>
                        <li><a href="{{ URL::to('skills') }}/Environmental Science & Sustainability">Environmental Science & Sustainability</a></li>
                        <li><a href="{{ URL::to('skills') }}/Chemistry">Chemistry</a></li>
                    </ul>
                    
   </div>
   <div class="col-md-4">
      <ul class="colps">
                        <li><a href="{{ URL::to('skills') }}/Physics">Physics</a></li>
                        <li><a href="{{ URL::to('skills') }}/Advertising & Journalism">Advertising & Journalism</a></li>
                        <li><a href="{{ URL::to('skills') }}/Education">Education</a></li>
                        <li><a href="{{ URL::to('skills') }}/Social Sciences">Social Sciences</a></li>
                        <li><a href="{{ URL::to('skills') }}/Psychology & Behavioral Sciences">Psychology & Behavioral Sciences</a></li>
                        <li><a href="{{ URL::to('skills') }}/Political Science and Government">Political Science and Government</a></li>
                    </ul>
   </div>
  
   </div>
  </div>
</div>
<object  id="svgObject" type="image/svg+xml" data="{{asset('assets/world_border.svg')}}" style="width: 100%;height: 100%;    background-color: #CCCCCC;">Your browser does not support SVG</object>
<script>
   $(window).load(function () {
    var a = document.getElementById("svgObject");
    var svgDoc = a.contentDocument;
    $.get( "fillpath", function(data){
        $.each(data,function(idx,obj){
            if(obj.filled == "1"){
                //console.log(obj.name);
                var delta = svgDoc.getElementById(obj.country_id); //get the inner element by id
                delta.style.fill= "#a83334";
            }
            

        });
    });
});

   function newlink(){
    console.log("entered");
   }

   $('.fillt').click(function(){
    $('#svgObject').toggleClass('filtt');
     $('#colss').toggleClass('filtext');
    
    });
</script>

@stop
